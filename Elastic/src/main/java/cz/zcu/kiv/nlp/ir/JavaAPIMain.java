package cz.zcu.kiv.nlp.ir;

import co.elastic.clients.elasticsearch.ElasticsearchClient;
import co.elastic.clients.elasticsearch._types.HealthStatus;
import co.elastic.clients.elasticsearch._types.query_dsl.Query;
import co.elastic.clients.elasticsearch.cluster.HealthResponse;
import co.elastic.clients.elasticsearch.cluster.health.IndexHealthStats;
import co.elastic.clients.elasticsearch.core.*;
import co.elastic.clients.elasticsearch.core.bulk.BulkOperation;
import co.elastic.clients.elasticsearch.core.bulk.IndexOperation;
import co.elastic.clients.elasticsearch.core.search.Hit;
import co.elastic.clients.elasticsearch.indices.*;
import co.elastic.clients.elasticsearch.indices.ExistsRequest;
import co.elastic.clients.json.JsonData;
import co.elastic.clients.json.jackson.JacksonJsonpMapper;
import co.elastic.clients.transport.ElasticsearchTransport;
import co.elastic.clients.transport.rest_client.RestClientTransport;
import org.apache.http.HttpHost;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.ssl.SSLContextBuilder;
import org.apache.http.ssl.SSLContexts;
import org.elasticsearch.client.RestClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.net.ssl.SSLContext;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;
import java.util.concurrent.ExecutionException;

/**
 * @author tigi, pauli
 */
public class JavaAPIMain {

    private static final Logger log = LoggerFactory.getLogger(JavaAPIMain.class);

    private static final String INDEX_NAME = "behej-com1";


    public static void main(String args[]) throws Exception {
        final CredentialsProvider credentialsProvider = new BasicCredentialsProvider();
        credentialsProvider.setCredentials(AuthScope.ANY,
                new UsernamePasswordCredentials("elastic", "elastic"));

        SSLContextBuilder sslBuilder = SSLContexts.custom()
                .loadTrustMaterial(null, (x509Certificates, s) -> true);
        final SSLContext sslContext = sslBuilder.build();

        RestClient restClient = RestClient.builder(
                new HttpHost("localhost", 9200, "https"))
                .setHttpClientConfigCallback(httpClientBuilder -> {
                    httpClientBuilder.setDefaultCredentialsProvider(credentialsProvider);
                    httpClientBuilder
                            .setSSLContext(sslContext)
                            .setSSLHostnameVerifier(NoopHostnameVerifier.INSTANCE);
                    return httpClientBuilder;
                }).build();

        ElasticsearchTransport transport = new RestClientTransport(
                restClient, new JacksonJsonpMapper());
        ElasticsearchClient client = new ElasticsearchClient(transport);


        printConnectionInfo(client);

        try {
            boolean exists = createIndexIfNotExist(client, INDEX_NAME);

            if(!exists) {
                // ----------- Uploading -------------- //
                log.debug("Uploading");
                setIndexMapping(client, INDEX_NAME);
                List<Record> records = loadRecords();
                if (records.isEmpty()) {
                    log.warn("No documents to index.");
                } else {
                    indexDocuments(records, client, INDEX_NAME);
                }
            }

            // ----------- Searching -------------- //
            log.debug("Searching");
            searchAuthor(client, INDEX_NAME);
            searchBool(client, INDEX_NAME);
            searchAuthorYear(client, INDEX_NAME);
            //searchTrumpGood(client, INDEX_NAME);
            //searchTrumpGoodFilterByScore(client, INDEX_NAME, 50);

            // ----------- Updating -------------- //
            log.debug("Updating");
            indexTestDocument(client, "test-index", "test-1");
            getDocument(client, "test-index", "test-1");
            updateDocument(client, "test-index", "test-1", "This is updated text.");
            getDocument(client, "test-index", "test-1");
            deleteDocument(client, "test-index", "test-1");
        } catch (Exception ex) {
            log.error("Exception: ", ex);
        }

        restClient.close();
    }

    private static boolean createIndexIfNotExist(ElasticsearchClient client, String indexName) throws IOException {
        log.info("Checking if index '{}' exists.", indexName);

        boolean exists = client.indices().exists(new ExistsRequest.Builder().index(indexName).build()).value();

        if (!exists) {
            log.info("Index does not exist, creating new one.");
            client.indices().create(new CreateIndexRequest.Builder().index(indexName).build());
        } else {
            log.info("Index already exist.");
        }

        return exists;
    }

    private static void setIndexMapping(ElasticsearchClient client, String indexName) throws IOException {
        log.info("Setting mapping in index '{}'.", indexName);
        InputStream input = new FileInputStream("mappings.json");

        PutMappingRequest request = new PutMappingRequest.Builder().index(indexName).withJson(input).build();
        client.indices().putMapping(request);

        log.info("Done.");
    }

    private static List<Record> loadRecords() {
        File file = new File("records_2022-03-01_23_08_226.json");
        return Record.load(file);
    }

    private static void indexDocuments(List<Record> records, ElasticsearchClient client, String indexName) throws IOException {
        BulkRequest.Builder request = new BulkRequest.Builder().index(indexName);
        int currentBulkSize = 0;
        int bulkCntr = 0;
        int cntr = 0;
        final int docCount = records.size();

        log.info("Indexing {} documents.", records.size());

        List<BulkOperation> operations = new ArrayList<>();
        for (Record record : records) {
            BulkOperation operation = new BulkOperation.Builder()
                    .index(new IndexOperation.Builder<Record>().index(indexName).document(record).build())
                    .build();
            operations.add(operation);

            currentBulkSize++;
            cntr++;

            if (currentBulkSize >= 50 || cntr >= docCount) {
                bulkCntr++;
                log.info("Executing bulk {} with {} documents.", bulkCntr, currentBulkSize);

                request.index(indexName);
                request.operations(operations);
                BulkResponse response = client.bulk(request.build());
                //for (BulkResponseItem item : response.items()) {
                //    System.out.println(item.error().reason());
                //}

                request = new BulkRequest.Builder();
                operations.clear();
                currentBulkSize = 0;
            }
        }
    }

    private static void indexTestDocument(ElasticsearchClient client, String indexName, String documentId) throws IOException {
        log.info("Indexing test document to index '{}'", indexName);
        Record record = new Record();
        record.setAuthor("Test autor");
        record.setDate("29.03.2022");
        record.setIntro("Test intro");
        record.setText("Test text");
        record.setTitle("Test title");
        record.setUrl("https://test-url.com");
        record.setRead("");
        record.setComments(new ArrayList<>());

        client.index(new IndexRequest.Builder<Record>()
                .index(indexName)
                .document(record)
                .id(documentId)
                .build());

        log.info("Done.");
    }

    private static void getDocument(ElasticsearchClient client, String index, String id) throws IOException {

        GetRequest getRequest = new GetRequest.Builder().index(index).id(id).build();
        GetResponse<Record> getResponse = client.get(getRequest, Record.class);

        if (!getResponse.found()) {
            log.info("Document with id:" + id + " not found");
            return;
        }

        Record item = getResponse.source();

        log.info("------------------------------");
        log.info("Retrieved document");
        log.info("Index: " + getResponse.index());
        log.info("Id: " + getResponse.id());
        if (item != null)
            log.info(item.toString());
        log.info("------------------------------");
    }

    static class RecordUpdate {
        String title;

        public RecordUpdate(String title) {
            this.title = title;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }
    }

    private static void updateDocument(ElasticsearchClient client, String index,
                                       String id, String newValue) throws IOException, ExecutionException, InterruptedException {
        log.info("Updating document '{}'.", id);

        UpdateRequest<Record, RecordUpdate> updateRequest = new UpdateRequest.Builder<Record,RecordUpdate>()
                .index(index)
                .id(id)
                .doc(new RecordUpdate(newValue))
                .build();

        client.update(updateRequest, Record.class);

        log.info("Done.");
    }

    private static void deleteDocument(ElasticsearchClient client, String index, String id) throws IOException {

        log.debug("Deleting document with id '{}'.", id);

        DeleteRequest deleteRequest = new DeleteRequest.Builder()
                .index(index)
                .id(id)
                .build();

        DeleteResponse response = client.delete(deleteRequest);
        log.info("Information on the deleted document:");
        log.info("Index: " + response.index());
        log.info("Id: " + response.id());
    }

    private static void searchAuthor(ElasticsearchClient client, String indexName) throws IOException {
        log.info("Performing search author query.");

        SearchRequest searchRequest = new SearchRequest.Builder()
                .index(indexName)
                .query(q -> q.match(m -> m.field("author").query("Miloš Škorpil"))).build();
                //.query(q -> q.term(t -> t.field("author").value(v -> v.stringValue("Miloš Škorpil")))).build();
        SearchResponse<Record> search = client.search(searchRequest, Record.class);
        printSearchResponse(search);
    }

    private static void searchAuthorYear(ElasticsearchClient client, String indexName) throws IOException {
        log.info("Performing search author year query.");

        SearchRequest searchRequest = new SearchRequest.Builder()
                .index(indexName)
                .query(q -> q.bool(b -> b
                .must(Query.of(m1 -> m1.match(m2 -> m2.field("author").query("Miloš Škorpil"))),
                        Query.of(m3 -> m3.range(r1 -> r1.field("date").gte(JsonData.of("01.01.2009"))))))).build();
        SearchResponse<Record> search = client.search(searchRequest, Record.class);
        printSearchResponse(search);
    }

    private static void searchBool(ElasticsearchClient client, String indexName) throws IOException {
        log.info("Performing search bool query.");

        SearchRequest searchRequest = new SearchRequest.Builder()
                .index(indexName)
                .query(q -> q.bool(b -> b
                .must(m1 -> m1.match(m2 -> m2.field("text").query("Homoláč")))
                .should(s -> s.match(m3 -> m3.field("text").query("maraton půmaraton 10km desítka")))
                .minimumShouldMatch("1"))).build();
        SearchResponse<Record> search = client.search(searchRequest, Record.class);
        printSearchResponse(search);
    }

    private static void printSearchResponse(SearchResponse<Record> search) {
        log.info("Search complete");
        log.info("Search took: " + search.took() + " ms");
        log.info("Found documents: " + search.hits().total().value());

        for (Hit<Record> hit: search.hits().hits()) {
            log.info("--------");
            log.info("Doc id: " + hit.id());
            log.info("Score: " + hit.score());
            log.info(hit.source().toString());
        }

        log.info("------------------------------");
        System.out.println("");
    }

    private static void printConnectionInfo(ElasticsearchClient client) throws IOException {
        HealthResponse health = client.cluster().health();
        String clusterName = health.clusterName();

        log.info("Connected to Cluster: " + clusterName);
        log.info("Indices in cluster: ");
        for (IndexHealthStats heal : health.indices().values()) {
            String index = heal.status().name();
            int numberOfShards = heal.numberOfShards();
            int numberOfReplicas = heal.numberOfReplicas();
            HealthStatus status = heal.status();

            log.info("Index: " + index);
            log.info("Status: " + status.toString());
            log.info("Number of Shards: " + numberOfShards);
            log.info("Number of Replicas: " + numberOfReplicas);
            log.info("---------");

        }
    }
}
