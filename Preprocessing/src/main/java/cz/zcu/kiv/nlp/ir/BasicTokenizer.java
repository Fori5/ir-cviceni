/*
  Copyright (c) 2014, Michal Konkol
  All rights reserved.
 */
package cz.zcu.kiv.nlp.ir;

/**
 * @author Michal Konkol
 */
public class BasicTokenizer implements Tokenizer {

    String splitRegex;

    public BasicTokenizer(String splitRegex) {
        this.splitRegex = splitRegex;
    }

    @Override
    public String[] tokenize(String text) {
        return text.split(splitRegex);
    }

    final String withDiacritics = "áÁčČďĎéÉěĚíÍňŇóÓřŘšŠťŤúÚůŮýÝžŽ";
    final String withoutDiacritics = "aAcCdDeEeEiInNoOrRsStTuUuUyYzZ";

    @Override
    public String removeAccents(String text) {
        for (int i = 0; i < withDiacritics.length(); i++) {
            text = text.replaceAll("" + withDiacritics.charAt(i), "" + withoutDiacritics.charAt(i));
        }
        return text;
    }
}
