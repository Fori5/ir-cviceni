package cz.zcu.kiv.nlp.ir;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.CharArraySet;
import org.apache.lucene.analysis.cz.CzechAnalyzer;
import org.apache.lucene.document.*;
import org.apache.lucene.index.*;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.*;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.*;

public class HelloLucene {
  private static final String INDEX_FILE = "records.index.lucene";
  private static final int HITS_PER_PAGE = 10;
  private static final int MAX_RESULTS = 20;
  private static final SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");


  public static void main(String[] args) throws IOException, ParseException {
    // 0. Specify the analyzer for tokenizing text.
    //    The same analyzer should be used for indexing and searching
    Analyzer analyzer = createAnalyzer();
    List<Record> records = loadRecords();

    // 1. create the index
    Directory index = getIndex(analyzer, records);

    // 2. queries
    Map<String, Query> queries = prepareQueries(analyzer);

    performQueries(queries, index);
  }


  private static void performQueries(Map<String, Query> queries, Directory index) throws IOException {
    IndexReader reader = DirectoryReader.open(index);

    for(String queryName : queries.keySet()) {
      performQuery(queryName, queries.get(queryName), reader);
    }

    reader.close();
  }

  private static void performQuery(String queryName, Query query, IndexReader reader) throws IOException {
    System.out.println("Performing query: "+queryName);
    int hitsPerPage = HITS_PER_PAGE;
    IndexSearcher searcher = new IndexSearcher(reader);
    int page = 0;
    ScoreDoc lastDoc = null;
    TopDocs docs = performQueryPage(query, searcher, hitsPerPage, page, lastDoc);
    ScoreDoc[] hits = docs.scoreDocs;

    while(hits.length > 0) {
      lastDoc = hits[hits.length-1];
      System.out.println("Page "+(page+1)+"\n"+hits.length+" hits.\n=======================");
      printPage(page, hits, searcher);

      page++;
      docs = performQueryPage(query, searcher, hitsPerPage, page, lastDoc);
      hits = docs.scoreDocs;
      System.out.println("=======================\n");
    }
    System.out.println("Done.\n\n\n");
  }

  private static void printPage(int page, ScoreDoc[] hits, IndexSearcher searcher) throws IOException {
    for(int i=0;i<hits.length;++i) {
      int docId = hits[i].doc;
      Document d = searcher.doc(docId);
      System.out.println((i+1)+": \nauthor: "+d.get("author")+"\ntitle: "+d.get("title")+"\ntext: "+d.get("text")+"\nLucene score: "+hits[i].score);
    }
  }

  private static TopDocs performQueryPage(Query q, IndexSearcher searcher, int hitsPerPage, int page, ScoreDoc lastDoc) throws IOException {
    TopScoreDocCollector collector;
    if (lastDoc != null) {
      collector = TopScoreDocCollector.create(MAX_RESULTS, lastDoc);
    } else {
      collector = TopScoreDocCollector.create(MAX_RESULTS);
    }

    searcher.search(q, collector);

    int startIndex = page*hitsPerPage;
    return collector.topDocs(startIndex, hitsPerPage);
  }

  private static Map<String, Query> prepareQueries(Analyzer analyzer) throws ParseException {
    System.out.println("Preparing queries.");
    Map<String, Query> queries = new HashMap<>();
    queries.put("Homoláč query", new QueryParser("text", analyzer).parse("Homoláč"));
    queries.put("Homoláč and Pavlišta query", new QueryParser("text", analyzer).parse("\"Homoláč\" AND \"Pavlišta\""));
    queries.put("Homoláč or Pavlišta query", new QueryParser("text", analyzer).parse("\"Homoláč\" OR \"Pavlišta\""));
    queries.put("Wildcard query", new QueryParser("text", analyzer).parse("běž*"));
    queries.put("Homoláč Kroměříž", new QueryParser("intro", analyzer).parse("intro:(\"Homoláč Kroměříž\"~5) OR text:(\"Homoláč Kroměříž\"~5)"));
    queries.put("Homoláč, Pavlišta and maraton", new QueryParser("text", analyzer).parse("(\"Homoláč\" OR \"Pavlišta\") AND \"maraton\""));
    queries.put("Homoláč, Pavlišta and maraton or půlmaraton", new QueryParser("text", analyzer).parse("(\"Homoláč\" OR \"Pavlišta\") AND (\"maraton\" OR \"půlmaraton\")"));
    queries.put("Post by Radek Narovec", new QueryParser("author", analyzer).parse("\"Radek Narovec\""));
    queries.put("Posts from 2021", LongPoint.newRangeQuery("datetime", 1609455600000L, 1640991599000L));
    return queries;
  }

  private static Directory getIndex(Analyzer analyzer, List<Record> records) throws IOException {
    Path p = Paths.get(INDEX_FILE);
    boolean indexExists = Files.exists(p);
    Directory index = FSDirectory.open(p);
    if(!indexExists) {
      System.out.println("No existing index found, creating new one.");
      IndexWriterConfig config = new IndexWriterConfig(analyzer);
      IndexWriter w = new IndexWriter(index, config);
      indexRecords(w, records);
      w.close();
    }

    return index;
  }

  private static void indexRecords(IndexWriter w, List<Record> records) throws IOException {
    System.out.println("Indexing "+records.size()+" records.");
    for(Record record : records) {
      Document doc = new Document();

      doc.add(new TextField("title", record.getTitle(), Field.Store.YES));
      doc.add(new TextField("intro", record.getIntro(), Field.Store.YES));
      doc.add(new TextField("text", record.getText(), Field.Store.YES));
      doc.add(new TextField("author", record.getAuthor(), Field.Store.YES));
      try {
        Date date = dateFormat.parse(record.getDate());
        doc.add(new LongPoint("datetime", date.getTime()));
      } catch (java.text.ParseException e) {
        e.printStackTrace();
      }

      w.addDocument(doc);
    }
  }

  private static List<Record> loadRecords() {
    File file = new File("records_2022-03-01_23_08_226.json");
    return Record.load(file);
  }

  private static Analyzer createAnalyzer() throws IOException {
    return new CzechAnalyzer(new CharArraySet(loadStopwords(), true));
  }

  private static Collection<String> loadStopwords() throws IOException {
    return Files.readAllLines(Paths.get("stopwords.txt"));
  }
}
