package cz.zcu.kiv.nlp.vs;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.io.*;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: tigi
 * Date: 13.11.12
 * Time: 10:25
 * To change this template use File | Settings | File Templates.
 */
public class Record implements Serializable {
    private String url;
    private String title;
    private String author;
    private String date;
    private String read;
    private String intro;
    private String text;
    private List<Comment> comments;

    public Record() {
    }

    public static class Comment {
        protected String author;
        protected String date;
        protected String comment;

        public String getAuthor() {
            return author;
        }

        public void setAuthor(String author) {
            this.author = author;
        }

        public String getDate() {
            return date;
        }

        public void setDate(String date) {
            this.date = date;
        }

        public String getComment() {
            return comment;
        }

        public void setComment(String comment) {
            this.comment = comment;
        }
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getRead() {
        return read;
    }

    public void setRead(String read) {
        this.read = read;
    }

    public String getIntro() {
        return intro;
    }

    public void setIntro(String intro) {
        this.intro = intro;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public List<Comment> getComments() {
        return comments;
    }

    public void setComments(List<Comment> comments) {
        this.comments = comments;
    }

    @Override
    public String toString() {
        return "Record{" +
                "title='" + title + '\'' +
                ", author='" + author + '\'' +
                ", date='" + date + '\'' +
                '}';
    }

    public static void save(String path, List<Record> list) {
        BufferedWriter writer = null;
        try {
            Gson gson = new GsonBuilder().setPrettyPrinting().create();
            writer = new BufferedWriter(new FileWriter(path));
            gson.toJson(list, writer);
            writer.flush();
        } catch (IOException i) {
            i.printStackTrace();
        } finally {
            if (writer != null) {
                try {
                    writer.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static List<Record> load(File jsonFile) {
        try {
            Gson gson = new Gson();
            Type listType = new TypeToken<ArrayList<Record>>() {
            }.getType();
            return gson.fromJson(new FileReader(jsonFile), listType);
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }
}
