/clanek/2152-pohar-behej-com-nemame-dobre-zpravy	
V loňském a předloňském roce se podařilo kromě ostřílených harcovníků postavit 
na start běžeckých závodů několik desítek noviců, pro které byl do té doby 
výstřel ze startovní pistole znám spíše jen z televize. To nás hřeje a těší. 
V paměti máme i prosincový večírek v Praze spojený s vyhlášením loňských 
nejlepších účastníků a předáním mnoha cen od sponzorů. Proč tedy letos ono 
rozhodnutí Pohár nedělat?

Snad nebude znakem slabosti si přiznat, že se nám nepodařilo pro letošní rok 
najít partnery, kteří by pomohli udržet dynamiku růstu našeho Poháru. Ten po 
počátečních opatrných krůčcích přerostl ve vcelku solidní celostátní pohár 
o třech stovkách registrovaných běžců. Byť na jednotlivé závody pochopitelně 
nepřijeli všichni pohároví účastníci, i tak se stávalo, že zvýšená účast 
pořadatelům dělala jisté potíže a ty samé, když ne horší, hrozily i letos.

Jeden příklad za všechny – ne všechny závody si mohou dovolit například 
elektronickou časomíru. Nám se nepodařilo dát dohromady dostatečný obnos, 
abychom tyto a podobné nároky spojené s početnějším startovním polem dokázali 
pořadatelům uhradit.

Stejně tak i naši partneři nevyhodnotili a nevyhodnocují účast na většině 
závodů jako ekonomickou, což je asi základní podmínka pro úspěch jakéhokoliv 
zdravého a dlouhodobého spojení sportu a peněz. Na vině je zejména stále malý 
počet běžců. Snad se podaří do příštích let nejen znásobit startovní pole, ale 
i přitáhnout na závody rodinný doprovod či jiné návštěvníky. I na tom chceme 
letos pracovat.

Je potom nesmírně obtížné vybruslit z onoho propletence, kde některé závody 
jsou „v rukou“ jedné sponzorující značky, zatímco za pohárem stojí značka jiná.

Měli jsme možnost uspořádat pohár s menšími závody, ale upřímně: necítili 
jsme, že by to mělo smysl. Vždyť již nyní je na české i moravské scéně několik 
desítek pohárů, z nichž o většině jste patrně nikdy neslyšeli. Naše ambice 
sahaly a sahají výše.

Chtěli bychom nicméně jedním dechem dodat, že se nic nemění na našem pevném a 
dlouhodobém odhodlání podporovat a zviditelňovat vše dobré, co se na české 
běžecké scéně děje. To ostatně bylo prapůvodní záminkou pro vznik Poháru 
behej.com – pomoci agilním pořadatelům k hojnější účasti a rovněž nasměrovat 
nové běžce tam, kde na ně čekají s otevřenou náručí. Věříme, že se to z velké 
části dařilo a bude nadále dařit.

Za nás vám můžeme slíbit, že si uvolněním rukou od ne vždy viditelné, zato 
však časově nesmírně náročné organizační práce kolem Poháru vytvoříme více 
prostoru pro to hlavní: tvorbu běžeckých webů a časopisu (jehož další číslo se 
nyní formuje).

Děkujeme touto cestou všem účastníkům minulých ročníků Poháru za podporu, 
účast a sympatie. Není všem dnům konec. Ať vám to letos závodí aspoň tak, 
jako loni!

 

Vážení čtenáři,

 děkujeme Vám za vaše příspěvky, návrhy a názory k otázce Poháru behej.com. 
Důvodem, proč jsme Vás nechali čekat, byla jednak naše další diskuse uvnitř 
týmu, ale – víkend nevíkend – i finišující práce na novém čísle časopisu. Ale 
zpátky k poháru:

 Věřte nám, že naše rozhodnutí padlo po dlouhé rozvaze nad VŠEMI aspekty, 
které uvádíte, ba i mnoha dalšími, které zde v diskusi nepadly a které 
považujeme spíše za naše interní, chcete-li provozní (nikoliv však menší). 

Celé naše rozhodování je o to citlivější, že Pohár považujeme za svou srdeční 
záležitost, která však začala NEÚMĚRNĚ zatěžovat celý tým behej.com, jehož 
primární a nejdůležitější rolí BYLO, JE a BUDE vydávání časopisu a správa webu. 
Už toto samo, chceme-li to dělat kvalitně a kvalitněji, s sebou nese nonstop 
práci, z níž je vždy vidět jen malý kousek – výsledek.

 Velmi si vážíme Vašich nabídek k pomoci s Pohárem, taktéž velmi zahřeje 
vyjádření díků od všech, pro které se dva roky Poháru staly motivací pro další 
běhání. Bohužel stejně tak zabolí poznámky, že Pohár pro letošní rok neotvíráme 
z nedostatku nadšení či dokonce jiných prapodivných důvodů. Všichni, kdo byť 
jen okrajově a na krátkou dobu nahlédli do chodu všeho kolem behej.com, vědí, 
že bez nadlidské porce nadšení a obětavosti bychom nebyli tam, kde jsme. 

Nevytrhávejme jednotlivosti z celku. Věřte, že jsme prošli bod po bodu a 
zvážili všechny aspekty, než jsme se rozhodli tak, jak jsme se nakonec rozhodli.


Velice Vás proto prosíme, abyste respektovali naše rozhodnutí a neupírali nám 
na něj právo. Rozhodně se k běhu všech forem a myšlenek neotáčíme zády, spíše 
naopak a nyní si děláme jakýsi vnitřní úklid proto, abychom byli ve formě právě 
ve chvílích, kdy se to od nás očekává nejvíce. Stejně jako vy, i my se chceme 
během bavit. A taky přitom trochu běhat…

 Za laskavé pochopení a za celý tým behej.com děkují 

Petr Kostovič a Radek Narovec.

/clanek/2278-leone	
První, co ti řeknu je, že nemáš naprosto žádný důvod být smutná a zklamaná. 
Vždyť jsi dokázala víc, než ti, kteří doposud o tom, že vůbec vyběhnou, jen 
mluví. Ale hlavně: ty jsi uběhla půlmaraton. Čas, v němž jsi ho absolvovala, 
není vůbec důležitý. Důležitá je pouze ta skutečnost, že jsi překonala všechny 
potíže, které ti tento konkrétní závod postavil do cesty, a do cíle ses dostala 
v čase, který byl rychlejší, než čas, na který byl závod vypsán.

Přečteš-li si svůj článek několikrát znovu, sama zjistíš, že jsi dokázala 
překonat sebe sama. Z toho, co uvádíš, jen namátkou vybírám teplo. Píšeš, že ho 
nemáš ráda a v létě ti působí potíže se v něm vůbec někam přemisťovat. Teplo, 
které přišlo ve chvíli, kdy závod začal, sice nebyly žádné třicítky, ale na 
lidský organizmus, zvyklý z předcházejících měsíců na poměrně nízké teploty, to 
byl sám o sobě šok. Naší termoregulaci trvá několik dní, než je schopná na tyto 
teplotní rozdíly reagovat. A ty ses v té chvíli rozeběhla ke svému dosud 
nejtěžšímu fyzickému testu v životě. Já říkám: „Klobouk dolů, že jsi doběhla do 
cíle.“ A to nejen pro tuto skutečnost.

Dalším faktorem, který ovlivnil tvůj úsudek o tom, jaký výkon považovat za 
úspěch, bylo to, že jsi vzala za vlastní očekávání, které na tebe, v dobré víře 
pomoci, přenesli jiní. Přenesli ho na tebe tím, že ti vyjadřovali podporu, 
z níž byla cítit velmi silná solidarita a prostá lidská láska. Mohu ti říci 
z vlastní zkušenosti a potvrdil by ti to nejeden přední špičkový sportovec, 
který byl tomuto pozitivnímu tlaku vystaven poprvé, že tohle je schopné zabít 
i vola. Vůbec nepřeháním. Tak to skutečně je. Vždyť si spočítej, kolik týdnů 
jsi byla vystavena tlaku. To, že tvé tělo reagovalo tím, že už před startem jsi 
měla enormně vysokou tepovou frekvenci, mluvilo samo za sebe. Zase to byl od 
tebe zásah do černého, když jsi na to ve svém článku upozornila.

V tom stresu, v němž jsi byla, je v podstatě každé fandění destruktivní, 
protože si to překládáš do vzkazu: „Věříme ti,“ zatímco by sis měla říkat: 
„věřím si, jsem dobrá, jsem hvězda.“ Tedy musí to být o tobě, musí to jít 
z tebe, musíš to být zkrátka ty, kdo je o tom přesvědčen. Tady bohužel na nás 
působí efekt „soused“. Soused říkal, soused má. Jen ty jsi to nemehlo, co nic 
nedokáže! Ve chvíli, kdy sis nasadila sluchátka, dav a jeho emoce najednou 
zmizely, stala ses sama sebou a bylo ti hned lépe.

Jestli jsi začátek přepálila či ne, nemohu hodnotit. Nebyl jsem u toho. Vím 
však velmi dobře, jak na člověka působí první účast v závodě. Můžeš si říkat 
milionkrát, že nevystřelíš s ostatními a stejně podlehneš tomu davovému 
šílenství. Pro člověka, který nemá žádnou zkušenost, je to v podstatě naprosto 
přirozená reakce.

Vezmi si situaci, kdy bude někde hořet. Lidé mohli být před tímto skutečným 
požárem mnohokrát na tuto situaci připravováni cvičnými poplachy a přesto ve 
chvíli, kdy se ocitnou v takovéto krizové situaci, se začnou chovat jako jiná 
zvířata na zemi. Prostě a jednoduše je ovládne pud sebezáchovy a běží se stádem.

Tady ses nedopustila chyby v závodě. Jak říkám, napoprvé tuhle situaci málokdo 
zvládne, ale dopustila ses chyby v tom, že jsi před půlmaratonem neběžela žádný 
závod, jedno jak dlouhý, ale závod. V něm by sis tuhle situaci již vyzkoušela a 
byla na ní připravená. Ale jinak to žádná tragédie není. Příště už budeš 
zkušenější a stejné chyby se nedopustíš.

Co říci na závěr? Články o tom, jak se někdo připravuje ke svému prvnímu 
velkému závodu, jsou velmi ceněné jak čtenáři, tak redakcí. Čtenáři se 
s hrdinou, v tvém případě hrdinkou, identifikují – vždyť je jedním z nich. 
V jeho přípravě jsou demonstrovány všechny chyby, jichž jsme se kdy dopouštěli 
a stále dopouštíme i my. Máme možnost přenést své touhy, své emoce do komentářů 
a povzbuzení, které dotyčnému adresujeme. A redakce vítá jejich obrovskou 
sledovanost.

Neodsuzuji ani jedno ani druhé, vždyť jsem se sám na tom podílel a věřím, že 
ještě určitě mnohdy budu. Jen je třeba si tyhle skutečnosti uvědomit. Toho, kdo 
do toho jde, bychom na to předem měli upozornit.

Nakonec bych chtěl vzkázat tobě Leono, i všem, kteří v sobotu uběhli svůj 
první půlmaraton v životě: „Jsi borec!“

Miloš Škorpil

 

O autorovi:
Běžec, poradce a propagátor běhání a zdravého životního stylu. Dvakrát oběhl 
Českou republiku, úspěšně zdolal Spartathlon – běh z Athén do Sparty, drží řadu 
rekordů v netradičních sportovních disciplínách. Každý týden vede tréninky 
v pražské Stromovce a ve fitku Školka na Praze 4 – Chodov a přestože ho nejvíce 
zaměstnává jeho Běžecká škola, nenechá ho lhostejným nic, co se kolem něj ve 
světě děje.

/clanek/15114-svatek-vrcharu-se-blizi-v-krkonosich-pujde-o-body-do-svetoveho-poharu-i-ceske-tituly	
„Loni nám zabránila v pořádání proticovidová opatření a celý seriál závodů byl 
následně zrušen. Letos se ale Janské Lázně stanou poprvé v zemích bývalého 
východního bloku dějištěm Světového poháru v běhu do vrchu,“ uvádějí pořadatelé 
půlmaratonu v pozvánce.

„Přidělení světového poháru je i poděkováním Českému atletickému svazu a 
organizátorům závodů k jubileu – 35 roků běhů do vrchu v Česku. Běhy do vrchu 
jsou velmi úspěšnou disciplínou, která má pravidelnou soutěž – Maratonstav 
Český poháru v běhu do vrchu. Pohár obsahuje 43 závodů v našich krásných horách 
po České republice.“



Krkonošský půlmaraton se poběží na trati z Janských Lázní přes Černou horu a 
zpět a bude mít délku 22 km s převýšením 1050 m. Závod se stal součástí TOP 
16 akcí světa nesoucí podtitul WMRA World Cup, mezi nimiž jsou například slavné 
závody Broken Arrow Skyrace v USA, nedávný horský běh Sierre – Zinal ve 
Švýcarsku či Grossglockner Berglauf v Rakousku. S ohledem na to, že do Krkonoš 
nemíří velká ekipa borců ze zahraničí, jsou body pro české běžce dostupnější 
než kdekoliv jinde. Nehledě na to, že půjde i o letenku na atraktivní světový 
šampionát, který by se měl koncem roku konat v exotickém Thajsku.

„Já se neskutečně na tento závod těším. Vidět Keňany vyběhnout po sjezdovce na 
Černou horu do bude zážitek! Je to hlavně odměna pro Karla Šklíbu za jeho 
35 roků organizování Českého poháru do vrchu,“ líčí Jindřich Linhart, 
reprezentační trenér vrchařů a technický delegát Českého atletického svazu. 
A současně upozorňuje, že lákadel pro domácí fandy vrchů je tentokrát opravdu 
hodně: „Krkonošský půlmaraton bude MČR v dlouhém horském běhu a zároveň MČR 
veteránů. Je také kvalifikací na listopadové MS v dlouhém horském běhu 
v Thajsku. Ze závodu v Janských Lázních budu nominovat první dva muže a 
dvě ženy.“

Přidává dlouhý výčet českých běžců, které v Krkonoších uvidíme: Pavla Schorná, 
Lucie Maršánová, Adéla Stránská, Petra Pastorová, Hana Stružková Švestková, 
Kateřina Matrasová, Magdalena Horká či Barbora Macurová. Pozadu nebudou chtít 
zůstat ani dvě stále výtečné veteránky Táňa Metelková a Ivana Sekyrová. Opravdu 
velká škoda, že nakonec ze startovky vypadla druhá v průběžném pořadí Světového 
poháru, Marcela Vašínová.

V mužích je situace obdobná, z těch nejlepších bude chybět jen Honza Janů – 
bude se v ten den ženit! A koho tedy uvidíme? Na startovce je Marek Chrascina, 
Vít Pavlišta, Petr Pechek, Ondřej Fejfar, Jiří Čípa, Tomáš Hudec či David 
Pelíšek. Tak schválně, na koho si vsadíte vy?

A ze zahraničních borců? „V této chvíli víme, že ve startovce je Keňanka Njeru 
Joyce Muthoni, vítězka druhého závodu SP na Grossglockneru, zatím šestá ve 
Světovém poháru. Potom sedmá žena SP a jedna z nejlepších světových běžkyň do 
vrchu, Irka Sarah McCormack. V mužích to jsou tři Keňané Ndungu Geofrfrey 
Gikuni, Kiptum Evans Kiprop a Kirui Timothy Kimutai, Slovinec Matic Plaznik, 
výborný bude i Maďar Sandor Szabo,“ informuje Linhart.

Pokud vás závod zaujal, vězte, že je otevřen veřejnosti a tudíž se po bok těch 
nejlepších můžete postavit i vy. Registrace jsou stále otevřeny: 
https://registrace.onlinesystem.cz/…rld-cup-2021 
<https://registrace.onlinesystem.cz/Season/81/mountain-running-world-cup-2021>

/clanek/8067-tony-krupicka-a-chris-wawrousek-o-vyvoji-new-balance-minimus	
Anton Krupicka vysvětluje filozofii vývoje řady NB Minimus.

Seříznutí NB Minimus

Tony provedl na svých botách NB790 celou řadu „úprav“, když začal výrazným 
seříznutím střední části podrážky, aby dosáhl téměř minimálního spádu mezi 
patou a středem podrážky. Seřízl jen něco přes centimetr jazyka a odstranil 
poutko na patě, protože „občas seschne, praskne a bota tam potom dře.“

Model MT100 zohledňuje většinu Tonyho postřehů, takže se jeho „úprava“ omezila 
už jen na seříznutí střední části podrážky – změna inspirovaná neutrální 
polohou chodidla, která se stala ústřední ideou řady NB Minimus.

Chris Wawrousek: „Pochopitelně, že nás vývoj řady NB Minimus jako návrháře 
doslova pohltil. Bryan Gothie [produktový manažer firmy New Balance] vzpomíná 
na úsměvnou příhodu, kdy v prodejně běžeckých potřeb potkal Tonyho [Anton 
Krupicka] a Kylea [Skaggs, rovněž z týmu New Balance]. Když se vrátil, 
rozpovídal se o těchto borcích a o tom, co všechno dělají. Všichni jsme 
nesmírně rádi, že máme možnost vytvářet produkt pro tak kvalitní atlety a navíc 
s jejich vlastní pomocí.

Svým způsobem je pro nás velice inspirativní, když se rovnou můžeme vrhnout na 
některé věci, které známe z trailového běhání. Tony a Kyle jsou přitom zástupci 
jiné skupiny ultraběhání. Tři nebo čtyři roky před zahájením spolupráce 
s těmito borci jsme provedli výzkum mezi ultramaratónci a došli k závěru, že 
boty, ke kterým směřovali, byly těžké a velké. Ti, co s nimi běhali, se pak 
docela trápili – nic, co by vás při pomyšlení na typického maratónce napadlo. 
Pak se ale objevili lidi typu Tonyho a Kylea s duchem opravdových závodníků a 
práce s nimi získala velice zajímavý směr.“

Tony Krupicka: „Co se mě týče, pracoval jsem před firmou New Balance s jiným 
výrobcem obuvi. Musím říct, že jsem byl na boty vždycky háklivý, alespoň pokud 
jde o tvar. Pro běžce je to jediný kus vybavení, na kterém skutečně záleží. 
Když jsem pak v Colorado Springs poznal Bryana, skutečně ho zajímalo, co jsme 
mu s Kyleem řekli na modely, co měl s sebou. Jen co jsem podepsal smlouvu, 
začal posílat boty, kterým jsme pak s Kyleem dávali pěkně zabrat. Bryan se 
ptal: „Co si o tom, kluci, myslíte?“ a my mu to všechno řekli. Bylo to vůbec 
poprvé, co nás tito chlápci opravdu poslouchali a zajímaly je naše názory. 
Takže jsem měl hned od začátku pocit, že cokoliv jim řeknu, nejdřív zváží, a 
ne, že to jen odkývnou a udělají si, co budou chtít. Je to prostě paráda a 
vážně s nimi rád dělám.“



NB.com: Kdy se tedy stalo, že Tonyho rady začaly ovlivňovat produktový vývoj 
řady MT100 a později i NB Minimus?

CW: „Řada 100 byla výsledkem vývoje boty, kterou jsme připravovali speciálně 
pro Tonyho a Kylea pro závodní účely. Jak se často stává, přijít s něčím tak 
radikálním na širší trh by bylo až příliš uspěchané. Coby návrhář jsem byl 
pochopitelně zklamaný, protože zpětná vazba, kterou jsme měli od Tonyho a 
Kylea, byla nanejvýš konkrétní – naposledy například takové detaily jako spád. 
To však byly věci, na něž tehdy ještě nebyl všední zákazník připravený. Pak se 
náhle objevila kniha Born to Run [bestseller Christophera McDougalla o dálkovém 
běhání], která výrazně podpořila myšlenku, že opravdu nejsme stavěni na běhání 
na klínech. A přesně to jsme slýchávali od Tonyho a Kylea, i když tehdy jsme to 
ještě nebyli schopni zúročit. Bylo tam spousta toho, o čem jsme již dříve 
s Tonym a Kyleem mluvili a co stálo za vznikem řady 100 – ta sice nenaplnila 
všechny naše představy, nicméně spousta tehdejších myšlenek dala později 
vzniknout řadě NB Minimus.“

NB.com: Tony, ještě jsme se nezmínili o jisté „úpravě“, kterou jsi provedl na 
svých běžeckých ‚stovkách‘. Do jaké míry ovlivnila vývoj řady NB Minimus?

TK: „No, ‚stovka‘ byla první botou, na které jsem se dost podílel a souběžně 
se dozvídal spoustu věcí o tvaru boty a její stavbě. S něčím jsem spokojený 
nebyl, jako třeba se způsobem, jakým byl svršek spojený s podrážkou, což jsem 
se nakonec pokusil vyřešit jejím seříznutím. Právě proto jsem řezal nejenom 
‚stovky‘, ale i ‚sedmsetdevade­sátky‘. Důvodem byla čistě jen snaha dosáhnout 
menšího spádu mezi patou a špičkou. Jak už řekl Chris, s řadou 100 jsme přišli 
o rok až dva dřív, takže k zákazníkovi jsme se dostat nemohli – ne každý se 
tehdy zajímal o skutečně rovnou a pružnou botu.

NB Minimus je ale naprosto jiná řada než ‚stovka‘. Je skvělá v tom, že opravdu 
umožňuje jistým způsobem se vyhranit od tradičních tvarů bot.“

CW: „Pochopitelně, že obsah jednání ve smyslu toho, co je přijatelná obuv, se 
od dob vývoje řady 100 po NB Minimus výrazně změnil. Před několika lety jsme 
přemýšleli nad otázkami typu „Můžeme posunout patní nárazovou podložku o 2 mm 
dopředu nebo už to příliš ovlivní středové vyvážení boty?“ Dnešní debaty na 
dané téma jsou zcela odlišné, otevřené a skutečně podnětné.“

NB.com: Jde tedy o opakovaný proces, kdy Tony dostane nový pár bot a podá vám 
zpětnou vazbu, načež Chrisova skupina tyto podněty zpracuje a učiní příslušné 
změny? Jak probíhá současná spolupráce?

TK: „Dostal jsem první návrhy bot NB Minimus, ale vždy šlo jen o nějaký nový 
typ podrážky se svrškem z modelu 100, aby se v tom dalo běhat. Vlastně ještě 
před tím jsem dostal jenom odlitek, tedy kopyto. Ve skutečnosti to spíš 
připomínalo pantofli. To bylo na samém počátku celého procesu. O botě jsme v té 
chvíli ještě nepřemýšleli, jen o tvaru kopyta. Potom mně poslali nákresy 
v CADu, které jsem okomentoval a poslal zpátky. Takže z mé strany to mezi námi 
bylo od začátku dost provázané.“

CW: „Někdy mám problémy oddělit dnešní NB Minimus od celého výzkumu, který 
provádíme od doby, kdy naše skupina Advanced Concept došla k závěru, že hlavním 
cílem je přirozený běh. Ony první návrhy byly ve skutečnosti jen zkušebním 
vzorkem vedoucím k porozumění otázky: co se stane, když změníte spád mezi patou 
a špičkou ze 6 mm na 3 mm a na 0 mm, a jak to změní styl běhu?“

NB.com: Tony, do jaké míry ses musel naučit jazyk návrhářů, aby ses mohl více 
zapojit do procesu vývoje konečného výrobku?

TK: „No, naučil jsem se něco o tvrdoměrech, svrchních materiálech a takových 
věcech, ale prakticky jsem jen říkal návrhářům, jak by to mělo vypadat a jak 
bych se v tom chtěl cítit. Postupně se ale naučíte, co znamená osmóza a všechny 
ostatní termíny.“

CW: „Tony o botách mluví skutečně dobře. Je naprosto jasné, co má na mysli.“

NB.com: Tony, pomáhají tvá doporučení při vývoji lepší obuvi pro závodění, 
trénink nebo obecně pro běh?

TK: „Pro mě je to především závodní obuv. Většinou totiž trénuji a závodím ve 
stejných botách. S modelem NB Minimus se však situace poněkud změnila – ve 
skutečnosti odpovídá mým názorům na běhání naboso –, protože se k běhání už 
nestavím jen ve stylu všechno nebo nic. Pokud denně trénuji v botách typu NB 
Minimus, které jsou super ohebné a skutečně nízké, pak mně to umožňuje závodit 
v botě, která je stejně tak lehká. V mém případě a s ohledem na terén, ve 
kterém běhám, nemusí NB Minimus poskytovat dostatečnou ochranu. Každodenní 
trénování v nich ale umožňuje závodit v tak lehké botě, v jaké by to 
jinak nešlo.

Samotná řada je svým způsobem rozdělená: model 100 představuje závodní botu 
vhodnou pro delší tratě, zatímco NB Minimus bych použil spíš při tréninku. 
Během několika posledních měsíců proto obě boty posuzuji i s ohledem na tuto 
skutečnost.“

Slovníček

Po našem rozhovoru jsme Chrise požádali o vysvětlení některých termínů, které 
zazněly. Zde je jeho vysvětlení.

(Obuvnické) kopyto

„Základní kámen. Ovlivňuje vše, co se objeví na výsledné botě, přesto už s ním 
ani leckteří veteráni obuvnického průmyslu nepracují. Výroba kopyta je podivnou 
směsicí umění a vědy připomínající alchymii nebo něco podobného.“

Nárazová plocha

„Část, která se shoduje s místem prvního dotyku nohy se zemí. Obvykle se 
nachází na patě, ale v případě bot NB Minimus se tento bod snažíme posunout 
více dopředu. Nejvíce patrné je to na modelu MR10. Jistým druhem ‚nárazové 
plochy‘ je i mírně zakřivená část gumy směrem ke špičce na vnější straně boty. 
Tento posun výrazně ovlivnila Tonyho úprava, ačkoliv dodnes s ním náš tým 
debatuje o tom, zda je tato změna přínosem, či nikoliv.“

Tvrdoměr

„Často se používá k popisu různých vlastností mezipodešve a podešve. Obecně 
řečeno jde o to, na co lidé poukazují při označení boty za ‚tvrdou‘ nebo 
‚měkkou‘. Z technického hlediska přitom mají pravdu – tvrdoměr (neboli 
durometr) měří tvrdost. Ve skutečnosti však stanovení ‚tvrdosti‘ nebo 
‚měkkosti‘ vychází z kombinace faktorů, do níž rovněž spadá chemická směs 
jednotlivých přísad, měrná tíha materiálu, geometrie, svrchní konstrukce a 
metoda kompletace.“

Modely Minimus najdete v nabídce těchto českých prodejen: Sport Kočárník 
<http://www.proe.cz/kocarnik2/>, Trailpoint <http://www.trailpoint.cz> a 
Triexpert <http://www.triexpert.cz>.

Více informací o značce New Balance najdete na stránkách jejího dovozce, firmy 
Aspire <http://www.newbalance.cz>.

/clanek/15215-poradna-do-zimniho-marastu-v-ponozkach-s-obsahem-merina	
Koncem loňského roku jsme otestovali třiadvacet modelů ponožek s převahou 
zahraničních značek. Střední, krátké, tenké, silnější… Do mokra (a na rychlejší 
trénink) se nejlépe osvědčily ty tenčí s převahou umělých materiálů. Nezadržují 
nadměrně vlhkost, navíc ve vhodné botě se může část vlhkosti z boty odvětrat. 
V mrazivém suchém počasí ale raději sáhněte po zateplených. Zde se projeví 
výhoda vlny merino, která udrží nohy v teple.

A právě obsah merina je asi největší změnou, kterou jsme zaznamenali od našeho 
obdobného testu v roce 2013. Tehdy bylo v testu pouhých pět modelů s tímto 
hřejivým materiálem, v tom aktuálním jich naopak jen necelá třetina vlnu merino 
neobsahovala! Prostě důraz na komfort a pocit tepla i v těch nejkrutějších 
zimách.

Pocit tepla ovlivňuje výběr ponožek na zimní běhání 
<https://top4running.cz/c/beh-bezecke-obleceni/type2-44/season-zima?a_box=z5d4p8nz>
 asi nejvíce. Ty nejlepší kousky povedeně kombinují vlnu merino s umělými 
materiály. Neměly by být příliš silné, aby nesnižovaly vnímání došlapu a 
zbytečně nezadržovaly vlhkost. Pokud vybíháte na objemový trénink, sáhněte po 
těch s výraznějším polstrováním chodidla, nejlépe ve spojení s plochými švy. 
Pro tempové tréninky a zimní závody se více hodí ponožky tenčí. Nesnižují cit a 
v pohodě v nich obujete i těsnější závodní boty.

JAK SE VYZNAT V MATERIÁLECH

Polyamid (PA) – vlákna vyrobená z ropy, vynikají vysokou pevností za sucha 
i mokra

Polypropylen (PP) – nejlehčí z textilních vláken, odolné proti oděru, 
nevytváří žmolky

Prolen – speciální polypropylenové vlákno, nesráží se, rychle schne

Polyester (PES) – vlákno zpravidla z PET materiálu, odolné na světle s malou 
navlhavostí a rychlým sušením

Elastan – vlákna se dají natáhnout až na trojnásobnou délku s návratem do 
původního rozměru, mají nízkou odolnost proti oděru, proto se používají pouze 
jako tzv. kombinované příze s jiným materiálem

  Nylon – první syntetické vlákno vyrobené z uhlí, vody a vzduchu, původně se 
používalo na výrobu kartáčů, později stálo za výrobou punčoch  

Bavlna – vlákno z plodu keře bavlníku, tvoří jej celulóza, dobře saje pot, má 
dobrou pevnost v tahu (za mokra dokonce o 20% větší)

Merino – vlna z horských ovcí, ve srovnání s běžnou vlnou je jemnější, má 
dobré izolační vlastnosti a je prodyšná

Co dalšího po ponožce požadovat? Především musí perfektně sedět na noze. Žádné 
shrnování nebo krabacení. V zimě chceme ochránit nejen chodidlo, ale 
i achilovku. Horní lem by měl být tedy dostatečně pevný, aby nedocházelo ke 
shrnování, zároveň pohodlný při delším běhu.

Dokonalému usazení na noze pomůže mírná komprese a umělé materiály. Ale 
pozor – pokud preferujete “umělinu”, pečlivě vybírejte velikost. Tu určuje 
délka chodidla, ne velikost boty! Většina modelů ponožek respektuje anatomickou 
odlišnost pravé a levé nohy, na chodidlo pak lépe padnou. Z pohledu životnosti 
však opakované namáhání ve stejném místě nemusí být výhodou. Také ploché švy 
napomáhají celkovému komfortu nohy. V oblasti prstů jej nabízí většina 
testovaných vzorků.

Naprosto zásadní je však spojení ponožky a boty, které výrazně ovlivňuje 
celkový komfort, funkčnost i životnost ponožky. Pokud zvolíte neprodyšnou botu, 
vlhkost zůstane uzavřená v botě i při použití funkční ponožky. Pokud běháte 
v silničních objemových botách o něco větších, pak s tenkou ponožkou riskujete 
klouzání nohy v botě. Naopak v silnějších ponožkách se nebudete po obutí 
těsnějších krosovek cítit nejlépe. Proměnných je zkrátka více a je potřeba 
volit vhodnou ponožku s ohledem na botu, počasí, délku a intenzitu běhu.

Co doporučujeme? Velikost volte u umělých materiálů raději větší (jsou 
těsnější), u ponožek s převahou materiálů přírodních naopak velikost menší 
(více se roztáhnou), u komprese rozhodně velikost větší (v zimě příliš silná 
komprese navozuje pocit chladu).

Na rychlé a krátké tempové tréninky (samozřejmě také závody) se více hodí 
tenké ponožky. Nesnižují pocit vnímání došlapu a při rychlém běhu vám zima 
nebude. Pro dlouhé běžecké tréninky, obzvláště pokud střídáte běh s chůzí, jsou 
vhodnější ponožky zateplené. Silnější polstrování při dlouhých bězích zvyšuje 
celkový komfort nohy.

Dejte si pozor na anomálie nohy (výrůstky, vybočení palců apod.), poškozují 
ponožku vyšším třením o botu, pro zvýšení životnosti používejte ponožky bez 
anatomického tvarování (levá, pravá), vydrží vám při nahodilém střídání déle.

Zimní ponožky můžete kombinovat s návleky. Zahřejí lýtko a zmírní otřesy 
u velmi dlouhých běhů.

Opatrně při praní v pračce. Pokud se ponožky potkají se suchým zipem, můžete 
je po vyprání prakticky vyhodit. Při běžném nošení a opatrném praní vám ale 
vydrží několik let!

NÁKUPEM NA TOP4RUNNING 
<https://top4running.cz/c/beh-bezecke-obleceni/type2-44/season-zima?a_box=z5d4p8nz>
 PODPOŘÍTE I NAŠI REDAKCI. PŘI NÁKUPU VLOŽTE KÓD BEHEJ5 A ZÍSKÁTE 5% SLEVU.

/clanek/15178-bechovice-byl-jsem-za-cerneho-kone-a-v-duchu-si-pral-medaili	
S čím jsi do běchovického závodu šel? Pomýšlel jsi na titul?

“Rozhodně jsem neměl ambice na první místo. Byl jsem brán jako černý kůň a 
nikdo moc neočekával, že bych mohl být první. Já osobně jsem si věřil na první 
pětku a v duchu jsem si přál medaili. Že to vyjde takhle dobře, jsem opravdu 
nečekal.”

Kde se v tobě vzalo tolik sil na drtivý finiš? Viktor se ani nesnažil ti 
sekundovat…

“Neřekl bych, že to byl nějaký extra finiš. Celý závod jsem se držel vzadu a 
snažil se šetřit sílu. Místo toho byl Viktor poměrně dlouhou dobu v čele a 
udával tempo. Já jsem se ho jen držel, a když jsem atakoval, tak už neměl 
dostatek energie. Možná to bylo i tím, že dva týdny zpátky běžel Vltava Run a 
hodně to tam řezal. Já tam byl sice taky, ale protože jsem věděl do čeho jdu, 
tak jsem to tolik nehrotil a běžel všechny úseky na dobrý pocit.”

Nestartoval Kuba Zemaník, myslíš, že bys měl i na něj?

“To nedokážu říct. Kuba je výborný běžec, a co předvedl minulý rok, bylo 
nádherný. Kdyby tam byl a začal to trhat kolem sedmého kilometru, tak bych se 
taky držel. Ale jak by to dopadlo potom, tak to nevím.”

Cítíš na desítce mimo dráhu ještě rezervu? Jsi pořád mladý…

“Určitě, jak už jsem říkal minulý rok, mám velké cíle. Chce to jen čas a 
správný trénink. Róbert Štefko, který je mým trenérem, odvádí výbornou práci a 
já mu věřím každé slovo. Vše má hlavu a patu, takže ani nemám potřebu nějak 
odporovat. Bere v potaz i můj náročný režim práce-škola. Chápe, že se musím 
něčím živit a že musím myslet na budoucnost. Proto dostávám tréninkové plány na 
měsíc dopředu, to mi hodně vyhovuje. Vždy se na nový těším. Zároveň věří, že má 
vše svůj čas a bere tréninkovou intenzitu pěkně postupně a myslí na budoucnost 
a na mé zdraví.”

Co vše se u tebe dělo od loňského léta, kdy jsme si podrobně povídali pro 
tištěné Běhej?

“Stalo se několik zásadních věcí. Změnil jsem trenéra a pracovní pozici. Dále 
jsem pomohl vytvořit super tréninkovou skupinu zvanou AKBK, která se skládá 
z výborných běžců. S nimi jsem absolvoval celou zimní přípravu, velkou část 
jarní přípravy a běháme spolu doteď minimálně klusy. Dále jsem si zaběhl kopu 
skvělých osobáku od 1000 m do 10 km. Nejvíce si cením osobáku na 3.000 m 8:17 a 
5.000 m 14:22. Mimo to jsem získal plno medailí a hlavně nová přátelství.” 

Nyní tě čeká půlmaraton ve Španělsku v Barceloně…

“Ano, přesně tak. Trenér mi řekl, že se můžu takhle na konci sezony vyzávodit 
na silnici a zaběhnout si, co budu chtít. Maraton byl moc dlouhý, a tak další 
jasnou volbou byl půlmaraton. Dostal jsem od Top4Running možnost startovat na 
půlmaratonu v Barceloně, a tak jsem to hned spojil. Podle toho jsem i uzpůsobil 
konec sezony, abych si mohl dostatečně odpočinout.”

Cítíš životní formu? Máš osobák 1:08:53, padne?

“Vím, že jsem na tom nejlépe, co jsem kdy byl. Výsledky to dokazují a nikdy 
jsem takhle neběhal. Proto ani nepochybuji, že si zaběhnu osobák. Otázkou pro 
mě spíš je, jak moc ho pokořím. Zároveň tomu ale nedávám moc velkou vážnost, 
jelikož to není trať, na kterou bych se specializoval. Trenér každopádně říkal, 
že bych měl zvládnout čas kolem 66 minut a 30 sekund. (pozn.red.: lépe za 
poslední tři roky běželi jen Jirka Homoláč, Vít Pavlišta a Jakub Zemaník) 
A vždycky, když mi předpovídal nějaký čas, tak se to potvrdilo.”

Kde tě dále letos uvidíme?

“Mimo půlmaraton v Barceloně budu startovat ještě na třech závodech. Bude se 
jednat o přespolní běh v Mnichově a pak MČR v přespolním běhu. Závěr roku 
ukončím na silvestrovském běhu. Jinak budu pokračovat v objemové přípravě.” 

Co jsou nyní tvé hlavní distance?

“Hlavní distancí je určitě trať na 5.000 m. Snažíme se ale zařazovat rychlost, 
a proto hledáváme závody například na 1.500 m. S mým osobákem to ale není 
lehké, a když jsem chtěl zkusit trať na 800 m, tak jsem bohužel nic nenašel, 
protože nemám zaběhnutý kvalitní čas, který by mě dostal do dobrého závodu.”

Co ty a maraton? Potřebujeme na této distanci generační obměnu.

“Ke konci sezóny jsem o něm lehce přemýšlel, ale věřím, že není kam spěchat. 
Až trenér řekne, že je čas ho vyzkoušet, tak do toho půjdu. Teď to ještě není. 
Stále mám velké mezery na krátkých tratích, takže je potřeba se zaměřit spíš 
na ně.”

Loni jsi zmiňoval nepříjemné zdravotní problémy, které tě limitovaly 
v progresu. Jak to vypadá nyní?

“Od roku 2019 jsem močil vždy po náročném tréninku a závodu krev. Žádný doktor 
si s tím nevěděl rady a říkali, že jsem v pořádku. Jenže to nebylo úplně lehké 
na moji psychiku a dost jsem s tím bojoval. Dokonce tak moc, že mě potkaly 
tuhle zimu i panické ataky. Pak jsem ale byl na vyšetření v Motole, kde mě 
pomohli výborní doktoři. Zjistili, že mám vrozenou vadu, která pravděpodobně 
způsobuje již zmiňovanou krev v moči. Od té doby se mi psychicky ulevilo a 
panické ataky přestaly, ale stále se muselo dál řešit, jestli půjdu na operaci. 
Pak to ale – to bylo po MČR na dráze ve Zlíně – najednou přestalo a od té doby 
to nesleduji, tak snad to tak zůstane.”

/clanek/14752-serial-mizuno-prague-park-race-ma-za-sebou-prvni-dva-zavody	
Oba závody spojovalo příjemné zázemí, tratě vedoucí krásnou přírodou, ale 
bohužel i nepříjemný déšť a rozbahnělá podložka. Závodníkům to ale moc 
nevadilo – na oba závody se jich vydalo téměř osm stovek! Navíc do náročného 
terénu mohli vyrážet v půjčených tréninkových, ale i závodních botách od 
generálního partnera závodu.



V Průhonicích ovládli přední místa závodníci z Mizuno teamu – nejrychlejší byl 
Jáchym Kovář, který pak v traťovém rekordu ovládl i kratší trasu v Divoké 
Šárce. Delší trasu v Šárce pak vyhrál Borek Požár (Mizuno team) před známým 
Skyrunnerem Tomášem Macečkem (Buff/Salamon team). Neskutečný výkon předvedla 
celkově třetí a v novém traťovém rekordu reprezentantka v bězích do vrchu 
Tereza Hrochová (AK Škoda Plzeň). V Průhonickém parku byla z žen nejrychlejší 
další česká reprezentantka Pavla Schorná (Mizuno team).



Na podzim pořadatelé z Prague Park Race chystají druhý ročník Mattoni FreeRun 
Konopiště (10.10.) – zájemci by neměli váhat s přihláškou, protože nejlevnější 
startovné pořídí pouze do konce června. Seriál Mizuno Prague Park Race pak 
tradičně uzavře pátý ročník závodu v Prokopském údolí (24.10.).

