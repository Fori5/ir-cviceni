Crawler (trida CrawlerBehej vyuzivajici selenium) prochází url adresy:
https://www.behej.com/clanky/1
https://www.behej.com/clanky/2
https://www.behej.com/clanky/3
atd

odkud bere odkazy na jednotlive clanky, napr:
https://www.behej.com/clanek/15264-dosahly-sporttestery-dokonalosti-a-budou-dalsi-inovace-sportu-ku-prospechu
Z kazdeho clanku bere:
- url
- nadpis
- autor
- datum
- pocet precteni
- intro (kratky text)
- text clanku
- komentare (autor, datum, text)

Vse se uklada do json souboru.